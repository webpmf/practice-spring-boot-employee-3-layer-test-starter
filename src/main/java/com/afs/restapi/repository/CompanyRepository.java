package com.afs.restapi.repository;

import com.afs.restapi.controller.CompanyController;
import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CompanyRepository {

    private static final List<Company> companies = new ArrayList<>();

    public CompanyRepository() {
        List<Employee> employeesInSpring = new ArrayList<>();
        employeesInSpring.add(new Employee(1, "alice", 21, "female", 6000));
        employeesInSpring.add(new Employee(2, "bob", 20, "male", 6200));
        employeesInSpring.add(new Employee(3, "charles", 22, "mfale", 5800));

        List<Employee> employeesInBoot = new ArrayList<>();
        employeesInBoot.add(new Employee(1, "daisy", 22, "female", 6100));
        employeesInBoot.add(new Employee(2, "ethan", 19, "male", 6000));

        companies.add(new Company(1, "spring", employeesInSpring));
        companies.add(new Company(2, "boot", employeesInBoot));
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public Company addCompany(Company company) {
        companies.add(new Company(generateNewId(), company.getCompanyName(), company.getEmployees()));
        return company;
    }

    private int generateNewId() {
        int maxId = companies.stream()
                .mapToInt(Company::getId)
                .max()
                .orElse(0);
        return maxId + 1;
    }

    public void clearAll() {
        companies.clear();
    }

    public Company getCompany(Integer companyId) {
        return getCompanies().stream()
                .filter(company -> company.getId().equals(companyId))
                .findFirst()
                .orElse(null);
    }

    public Company getUpdateCompany(Integer companyId, Company company, CompanyController companyController) {
        return getCompanies().stream()
                .filter(storedCompany -> storedCompany.getId().equals(companyId))
                .findFirst()
                .map(storedCompany -> companyController.updateCompanyAttributes(storedCompany, company))
                .orElse(null);
    }
}

