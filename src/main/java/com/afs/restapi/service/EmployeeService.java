package com.afs.restapi.service;

import com.afs.restapi.controller.EmployeeController;
import com.afs.restapi.exception.ThrowAgeErrorException;
import com.afs.restapi.exception.ThrowSalaryErrorException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    private EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public EmployeeRepository getEmployeeRepository() {
        return employeeRepository;
    }

    public List<Employee> getEmployees(EmployeeController employeeController) {
        return getEmployeeRepository().findAll();
    }

    public Employee getById(int id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> getByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> getByPage(int pageNumber, int pageSize) {
        return employeeRepository.findByPage(pageNumber, pageSize);
    }

    public Employee addEmployee(Employee employee) {
        if(employee.getAge()<18 || employee.getAge()>65){
            throw new ThrowAgeErrorException();
        }
        if(employee.getAge() == 31 && employee.getSalary() == 1000){
            throw new ThrowSalaryErrorException();
        }
        employee.setStatus(true);
        return employeeRepository.insert(employee);
    }

    public Employee getEmployee(int id, Employee employee) {
        return employeeRepository.update(id, employee);
    }

    public Employee getDelete(int id) {
        employeeRepository.delete(id).setStatus(false);
        return employeeRepository.delete(id);
    }
}
