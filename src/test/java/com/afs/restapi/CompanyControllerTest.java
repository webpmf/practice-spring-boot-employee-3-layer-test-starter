package com.afs.restapi;

import com.afs.restapi.exception.NotFoundException;
import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CompanyControllerTest {

    @Autowired
    MockMvc mockMvc;
    ObjectMapper mapper = new ObjectMapper();
    @Autowired
    CompanyRepository companyRepositoryRepository;

    @BeforeEach
    void setUp() {
        companyRepositoryRepository.clearAll();
    }

    @Test
    void should_get_all_companies_when_perform_get_given_companies() throws Exception {
        //given
        Company newCompany = buildCompanyTest1();
        companyRepositoryRepository.addCompany(newCompany);

        //when
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/companies"))
                .andReturn();

        //then
        MockHttpServletResponse response = mvcResult.getResponse();
        assertEquals(200, response.getStatus());
        List<Company> companies = mapper.readValue(response.getContentAsString(), new TypeReference<List<Company>>() {
        });
        assertEquals(1, companies.size());
        Company companyGet = companies.get(0);
        assertEquals(1, companyGet.getId());
        assertEquals("test1", companyGet.getCompanyName());
    }

    @Test
    void should_get_all_employees_when_perform_get_given_companyId() throws Exception {
        //given
        Company newCompany1 = buildCompanyTest1();
        Company newCompany2 = buildCompanyTest2();
        Company newCompany3 = buildCompanyTest2();
        companyRepositoryRepository.addCompany(newCompany1);
        companyRepositoryRepository.addCompany(newCompany2);
        companyRepositoryRepository.addCompany(newCompany3);

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/1/employees"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].name").isString())
                .andExpect(jsonPath("$[0].name").value("Susan"))
                .andExpect(jsonPath("$[0].age").isNumber())
                .andExpect(jsonPath("$[0].age").value(22))
                .andExpect(jsonPath("$[0].gender").isString())
                .andExpect(jsonPath("$[0].gender").value("Female"))
                .andExpect(jsonPath("$[0].salary").isNumber())
                .andExpect(jsonPath("$[0].salary").value(10000))
                .andExpect(jsonPath("$[0].companyId").value(1));
    }

    private static Company buildCompanyTest1() {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee(1, "Susan", 22, "Female", 10000,1));
        Company newCompany = new Company(1, "test1", employees);
        return newCompany;
    }

    private static Company buildCompanyTest2() {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee(2, "Lisi", 22, "Female", 10000,2));
        Company newCompany = new Company(2,"test2",employees);
        return newCompany;
    }

    private static Company buildCompanyTest3() {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee(3, "Zhangsan", 22, "male", 20000,3));
        Company newCompany = new Company(3,"test2",employees);
        return newCompany;
    }

    @Test
    void should_get_all_companies_when_perform_get_given_companies_use_matchers() throws Exception {
        //given
         Company newCompany = buildCompanyTest1();
         companyRepositoryRepository.addCompany(newCompany);

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].companyName").isString())
                .andExpect(jsonPath("$[0].companyName").value("test1"));
    }

    @Test
    void should_return_company_test1_with_id_1_when_perform_get_by_id_given_companies_in_repo() throws Exception {
        //given
        Company company1 = buildCompanyTest1();
        Company company2 = buildCompanyTest2();
        companyRepositoryRepository.addCompany(company1);
        companyRepositoryRepository.addCompany(company2);

        //when then
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.companyName").isString())
                .andExpect(jsonPath("$.companyName").value("test1"));
    }

    @Test
    void should_return_companies_by_page_and_size_when_perform_get_by_page_and_size_given_companies_in_repo() throws Exception {
        //given
        Company company1 = buildCompanyTest1();
        Company company2 = buildCompanyTest2();
        Company company3 = buildCompanyTest3();
        companyRepositoryRepository.addCompany(company1);
        companyRepositoryRepository.addCompany(company2);
        companyRepositoryRepository.addCompany(company3);

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies?pageIndex=1&pageSize=2"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].companyName").isString())
                .andExpect(jsonPath("$[0].companyName").value("test1"))
                .andExpect(jsonPath("$[1].id").isNumber())
                .andExpect(jsonPath("$[1].id").value(2))
                .andExpect(jsonPath("$[1].companyName").isString())
                .andExpect(jsonPath("$[1].companyName").value("test2"));
    }

    @Test
    void should_return_company_save_with_id_when_perform_post_given_a_company() throws Exception {
        //given
        Company companyTest1 = buildCompanyTest1();
        String test1Json = mapper.writeValueAsString(companyTest1);

        //when
        mockMvc.perform(MockMvcRequestBuilders.post("/companies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(test1Json))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.companyName").isString())
                .andExpect(jsonPath("$.companyName").value("test1"));
        Company companySaved = companyRepositoryRepository.getCompany(1);
        assertEquals(companyTest1.getId(), companySaved.getId());
        assertEquals(companyTest1.getCompanyName(), companySaved.getCompanyName());
    }

    @Test
    void should_update_company_in_repo_when_perform_put_by_id_given_company_in_repo_and_update_info() throws Exception {

        //given
        Company companyTest1 = buildCompanyTest1();
        companyRepositoryRepository.addCompany(companyTest1);
        Company toBeUpdateTest1 = buildCompanyTest1();
        toBeUpdateTest1.setCompanyName("updateName");
        String susanJson = mapper.writeValueAsString(toBeUpdateTest1);

        //when
        mockMvc.perform(MockMvcRequestBuilders.put("/companies/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(susanJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.companyName").isString())
                .andExpect(jsonPath("$.companyName").value("updateName"));
        Company test1InRepo = companyRepositoryRepository.getCompany(1);
        assertEquals(toBeUpdateTest1.getCompanyName(), test1InRepo.getCompanyName());
    }

    @Test
    void should_del_employees_and_company_in_repo_when_perform_del_by_id_given_companyId() throws Exception {
        //given
        Company companyTest1 = buildCompanyTest1();
        companyRepositoryRepository.addCompany(companyTest1);

        //when
        mockMvc.perform(MockMvcRequestBuilders.delete("/companies/{id}", 1));

        //then
        Assertions.assertNull(companyRepositoryRepository.getCompany(1));
    }
}