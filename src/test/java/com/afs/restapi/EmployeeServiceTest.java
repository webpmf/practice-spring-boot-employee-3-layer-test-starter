package com.afs.restapi;

import com.afs.restapi.exception.NotFoundException;
import com.afs.restapi.exception.ThrowAgeErrorException;
import com.afs.restapi.exception.ThrowSalaryErrorException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.EmployeeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class EmployeeServiceTest {

    private EmployeeRepository employeeRepository;

    private EmployeeService employeeService;

    @BeforeEach
    void setUp() {
        employeeRepository = mock(EmployeeRepository.class);
        employeeService = new EmployeeService(employeeRepository);
    }

    @Test
    void should_throw_age_error_when_insertEmployee_given_age18_and_age65() {
        //given
        Employee employeeZhangsan = new Employee(1, "Zhangsan", 17, "man", 1000);
        Employee employeeLisi = new Employee(2, "Lisi", 66, "man", 100000);

        //when
        //then
        Assertions.assertThrows(ThrowAgeErrorException.class,()->employeeService.addEmployee(employeeZhangsan));
        Assertions.assertThrows(ThrowAgeErrorException.class,()->employeeService.addEmployee(employeeLisi));

        verify(employeeRepository,times(0)).insert(any());


    }

    @Test
    void should_throw_salary_error_when_insertEmployee_given_age31_salary1000() {
        //given
        Employee employeeZhangsan = new Employee(1, "Zhangsan", 31, "man", 1000);

        //when then
        Assertions.assertThrows(ThrowSalaryErrorException.class,()->employeeService.addEmployee(employeeZhangsan));
    }

    @Test
    void should_return_employee_with_status_true_when_insert_employee_given_an_employee_match_rule1() {
        //given
        Employee employee = new Employee(1, "Zhangsan", 19, "man", 20000);

        //when
        employeeService.addEmployee(employee);

        //then
        verify(employeeRepository).insert(argThat(employeeToSaved->{
            assertTrue(employeeToSaved.isStatus());
            return true;
        }));
    }

    @Test
    void should_return_employee_with_status_true_when_insert_employee_given_an_employee_match_rule2() {
        //given
        Employee employee = new Employee(1, "Zhangsan", 19, "man", 20000);
        Employee employeeToReturn = new Employee(1, "Zhangsan", 19, "man", 20000);
        employeeToReturn.setStatus(true);
        when(employeeRepository.insert(any())).thenReturn(employeeToReturn);

        //when
        Employee employeeSaved = employeeService.addEmployee(employee);

        //then
        assertTrue(employeeSaved.isStatus());
    }

    @Test
    void should_set_employee_status_false_when_delete_employee_given_employeeId1() {
        //given
        Employee employee = new Employee(1, "Zhangsan", 19, "man", 30000);
        Employee employeeToReturn = new Employee(1, "Zhangsan", 19, "man", 20000);
        employeeService.addEmployee(employee);
        employeeToReturn.setStatus(false);
        when(employeeRepository.delete(anyInt())).thenReturn(employee);

        //when
        employeeService.getDelete(1);

        // then
        assertFalse(employee.isStatus());
    }

    @Test
    void should_set_employee_status_false_when_delete_employee_given_employeeId2() {
        //given
        Employee employee = new Employee(1, "Zhangsan", 19, "man", 20000);
        employeeService.addEmployee(employee);
        Employee employeeToReturn = new Employee(1, "Zhangsan", 19, "man", 20000);
        when(employeeRepository.delete(anyInt())).thenReturn(employeeToReturn);

        //when
        Employee employeeDeleted = employeeService.getDelete(1);
        ;

        //then
        verify(employeeRepository).insert(argThat(employeeToSaved -> {
            assertFalse(employeeDeleted.isStatus());
            return true;
        }));
    }

    @Test
    void should_throw_error_when_update_employee_given_employeeId1_with_status_false() throws Exception {

        //given
        Employee employee = new Employee(1, "Zhangsan", 19, "man", 20000);
        employeeService.addEmployee(employee);
        employee.setStatus(false);
        Employee employeeToReturn = new Employee(1, "Zhangsan", 19, "man", 20000);
        when(employeeRepository.update(anyInt(),any())).thenThrow(new NotFoundException());

        //when
        //then
        assertThrows(NotFoundException.class, () -> employeeService.getEmployee(1,employee));

    }


}